## Contributing to yp-learning/100python

This repo is for learnings of [@yashppawar](https://gitlab.com/yashppawar), 
so it's not likely that the MR/PR will be 
merged (but they are still open), if you 
have any suggestions feel free to create 
an [issue](https://gitlab.com/yp-learning/100python/-/issues).

If you still want to create a MR, Thank you 
for showing interest in this Repo, and i'll 
try to get to you as soon as possible.

# 😊 Thank you.
